const beginButton = document.getElementById('begin')
const nextButton = document.getElementById('next')
const questionText = document.getElementById('question')
const answerButtons =  document.getElementById('answers')

let shuffledQuestions, currentQuestion
let score = 0 


// check if either of these buttons is being pressed
beginButton.addEventListener('click', beginGame)
nextButton.addEventListener('click', () =>{
	currentQuestion ++
	nextQuestion();
})


// begin the game
function beginGame(){
	// add the hide class so you don't see the start button anymore
	beginButton.classList.add('hide')

	// Remove the hide class from the objects that I need
	questionText.classList.remove('hide')
	answerButtons.classList.remove('hide')


	// shuffle the questions and go to the first one
	shuffledQuestions = questions.sort(() => Math.random() - .5)
	currentQuestion = 0
	nextQuestion()

}

function nextQuestion(){
	// Reset the previous data
	resetAll()

	// display the question
	showQuestion(shuffledQuestions[currentQuestion]);
}

function showQuestion(question) {
	// put the question text in the HTML
	questionText.innerText = question.question

	// loop through all the answers
	question.answers.forEach(answer => {
		
		// create a button for each answer
		const button = document.createElement('button')
		button.innerText = answer.text
		button.classList.add('btn')

		// check if the answer is correct
		if(answer.correct){
			button.dataset.correct = answer.correct
		}


		// display all buttons
		answerButtons.appendChild(button)

		// check if an answer is being clicked
		button.addEventListener('click', chooseAnswer)

	})
}


function resetAll() {
 // clear the color from the previous answer
 clearStatusclass(document.body)

 // hide the next button
 nextButton.classList.add('hide')

 // remove all answers
 while(answerButtons.firstChild){
 	answerButtons.removeChild(answerButtons.firstChild)
 }
}

function chooseAnswer(e){
	// get the chosen answer
	const selectedButton = e.target

	// get the correct value for the answer
	const correct = selectedButton.dataset.correct

	// change the body color to the color for right or wrong
	setStatusClass(document.body, correct)

	// change each button color to the the color for right or wrong
	Array.from(answerButtons.children).forEach(button =>{
		setStatusClass(button, button.dataset.correct)
	})

	// if the answer was correct increase the score
	if(correct){
		score++
	}

	// if this was not the last question show the next button
	if(shuffledQuestions.length > currentQuestion + 1){
		nextButton.classList.remove('hide');
	} 

	// otherwise remove the buttons and display the score 
	else{
		answerButtons.classList.add('hide')
		questionText.innerText = "YOUR SCORE IS: " + score + "/" + shuffledQuestions.length
	}

}




function setStatusClass(element, correct){
	// clear the previous value
	clearStatusclass(element)

	// check if correct and change class
	if(correct){
		element.classList.add('correct')
	}else{
		element.classList.add('wrong')	
	}
}


function clearStatusclass(element) {

	// clear the previous set classes
	element.classList.remove('correct');
	element.classList.remove('wrong');
}	



const questions = [
	{
		question: 'In which city was Justin born?',
		answers: [
			{text:"Amsterdam", correct: true},
			{text:"Hoorn", correct: false},
			{text:"Alkmaar", correct: false},
			{text:"Almelo", correct: false}
		]
	},

	{

		question: 'When did Justin get his diploma in Application development?',
		answers: [
			{text:"1998", correct: false},
			{text:"2016", correct: false},
			{text:"2019", correct: true},
			{text:"2021", correct: false}
		]

	},

	{

		question: 'How far away does Justin live from Social blue in Hoorn?',
		answers: [
			{text:"A 3 hour car ride", correct: false},
			{text:"A 2 hour bike ride", correct: false},
			{text:"A 30 mins car ride", correct: false},
			{text:"A 15 mins walk", correct: true}
		]

	},

	
	{

		question: 'In which year was Justin born?',
		answers: [
			{text:"1845", correct: false},
			{text:"1996", correct: true},
			{text:"2002", correct: false},
			{text:"2020", correct: false}
		]

	},

	
	{

		question: 'How excited would Justin be to get this job?',
		answers: [
			{text:"He doesn't really care", correct: false},
			{text:"VERY EXCITED!", correct: true},
		]

	}
]